﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stylelabs.Shutterfly.ImportExample
{
    public class LicenseResult
    {
        [JsonProperty(PropertyName = "license_id")]
        public string LicenseId { get; set; }

        [JsonProperty(PropertyName = "license_plate")]
        public string LicensePlate { get; set; }

        [JsonProperty(PropertyName = "license_state")]
        public string LicenseState { get; set; }

        [JsonProperty(PropertyName = "images")]
        public List<Image> Images { get; set; }
    }

    public class Image
    {
        [JsonProperty(PropertyName = "image_id")]
        public string ImageId { get; set; }

        [JsonProperty(PropertyName = "img_name")]
        public string ImageName { get; set; }

        [JsonProperty(PropertyName = "img_thumb_name")]
        public string ImageThumbName { get; set; }
        
        [JsonProperty(PropertyName = "license_id")]
        public string Licenseid { get; set; }
    }
}
