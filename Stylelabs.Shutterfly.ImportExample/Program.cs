﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stylelabs.M.Base.Web.Api.Models;
using Stylelabs.M.Sdk.WebApiClient;
using Stylelabs.M.Sdk.WebApiClient.Models;
using Stylelabs.M.Sdk.WebApiClient.Wrappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Stylelabs.Shutterfly.ImportExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //Get information
            var licenses = GetLicenses();

            CreateLicenses(licenses).Wait();

        }

        private static async Task CreateLicenses(List<LicenseResult> licenses)
        {
            var client = new MClient(new Uri("https://shutterfly-300.stylelabs.io/"),
                "AzureFunctions",
                "aslkdjf387fugc76u",
                "Administrator",
                "22628da2-dc2f-5721-b");

            foreach (var license in licenses)
            {
                var id = await CreateLicenseEntity(client, license);
                if (license.Images.Count > 0)
                {
                    Console.WriteLine($"http://www.beepcred.danieljrobles.com/images/{license.LicenseId}/{license.Images[0].ImageName}");
                    await CreateFetchJob(id, $"http://www.beepcred.danieljrobles.com/images/{license.LicenseId}/{license.Images[0].ImageName}", client);
                }
            }
        }


        private static async Task<long> CreateLicenseEntity(MClient client, LicenseResult test)
        {
            Console.WriteLine($"Create license {test.LicensePlate}");
            var licenseDefinition = await client.EntityDefinitions.Get("Ex.License").ConfigureAwait(false);

            var license = new EntityResourceWrapper(client);

            license.Resource.Properties = new Dictionary<string, JToken> {
                { "LicenseName", test.LicensePlate },
                { "LicenseState", test.LicenseState }
            };

            var result = await client.Entities.Create(license, licenseDefinition.Name).ConfigureAwait(false);

            return await CreateAsset(client, result);
        }

        private static async Task<long> CreateAsset(MClient client, long licenseId)
        {
            Console.WriteLine("Create Asset");
            var assetDefinition = await client.EntityDefinitions.Get("M.Asset").ConfigureAwait(false);

            var asset = new EntityResourceWrapper(client);

            asset.Resource.Properties = new Dictionary<string, JToken> {
                { "Title", "Example" }
            };

            asset.Resource.Relations.Add(
                "LicenseToAsset",
                new RelationResource() { Parent = new EntityLink("https://shutterfly-300.stylelabs.io/api/entities/" + licenseId) }
           );

            asset.Resource.Relations.Add(
                "FinalLifeCycleStatusToAsset",
                new RelationResource() { Parent = new EntityLink("https://shutterfly-300.stylelabs.io/api/entities/535") }
            );

            asset.Resource.Relations.Add(
                "ContentRepositoryToAsset",
                new RelationResource() { Parents = new List<EntityLink> { new EntityLink("https://shutterfly-300.stylelabs.io/api/entities/727") } }
           );

            var result = await client.Entities.Create(asset, assetDefinition.Name).ConfigureAwait(false);

            return result;
        }

        private async static Task CreateFetchJob(long assetId, string blobUrl, MClient client)
        {
            Console.WriteLine("Create fetch job");
            var test = new WebFetchJobRequest("Automatic from blob storage.", assetId) { Urls = new List<string> { blobUrl } };
            await client.Jobs.CreateFetchJob(test).ConfigureAwait(false);
        }

        private static List<LicenseResult> GetLicenses()
        {
            var baseUrl = "http://www.beepcred.danieljrobles.com/index.php/api/licenses";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
            request.Method = "GET";
            //request.Headers.Add(Constants.HeaderNames.Authorization, Settings.LaunchPad.Authentication);


            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            var result = string.Empty;

            var serializer = new JsonSerializer();
            using (var sr = new StreamReader(resStream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                JObject obj = (JObject)serializer.Deserialize(jsonTextReader);

                result = obj.ToString();

                var data = obj["data"]["result_array"];

                var results = JsonConvert.DeserializeObject<List<LicenseResult>>(data.ToString());

                return results ?? null;
            }
        }
    }
}
